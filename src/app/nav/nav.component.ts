import { Component } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: [
    '../app.component.css',
    './nav.component.css'
  ]
})
export class NavComponent {

  logout() {
    localStorage.removeItem('authUtils');
  }

}
