import {Component, Input} from '@angular/core';
import {AuthUtils} from "./AuthUtils";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    '../app.component.css',
    './login.component.css'
  ]
})
export class LoginComponent {

  @Input() username: string = '';
  @Input() password: string = '';
  error: string = '';
  authUtils: AuthUtils;

  constructor() {
    this.authUtils = new AuthUtils();
  }

  async handleSubmit() {
    this.error = '';
    try {
      const response = await fetch('/auth', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `grant_type=password&client_id=employee-management-service&username=${this.username}&password=${this.password}`
      });
      const body = await response.json();
      this.setAuthUtils(body);
      localStorage.setItem('authUtils', JSON.stringify(this.authUtils));
    } catch (e) {
      this.error = 'Benutzername und/oder Passwort sind inkorrekt!';
    }
  }

  setAuthUtils(response: any) {
    this.authUtils.token = response.access_token;
    this.authUtils.expiresIn = (Date.now()) + (response.expires_in * 1000);
  }

}
