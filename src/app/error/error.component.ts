import {Component, Input, OnInit} from '@angular/core';
import {Employee} from "../Employee";
import {ValidatorService} from "../validator.service";

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: [
    '../app.component.css',
    '../app.manageUser.css',
    './error.component.css'
  ]
})
export class ErrorComponent implements OnInit {
  @Input() employee: Employee | undefined | null = null;
  @Input() location: any;

  constructor(public validatorService: ValidatorService) {
  }

  ngOnInit(): void {
  }

  redirectToPreviousLocation(): void {
    this.location.back();
  }

}
