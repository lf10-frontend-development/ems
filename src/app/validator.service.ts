import {Injectable} from '@angular/core';
import {Employee} from "./Employee";

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {
  private readonly POSTCODE_LENGTH = 5;

  public isInvalidPostcode(property: any) {
    if (this.isNotDefined(property)) {
      return true;
    }
    return property.length !== this.POSTCODE_LENGTH;
  }

  public isNotDefined(object: any): boolean {
    return object === undefined || object === '';
  }

  public isValid(employee: Employee) {
    return (
      !this.isInvalidPostcode(employee.postcode) &&
      !this.isNotDefined(employee.city) &&
      !this.isNotDefined(employee.firstName) &&
      !this.isNotDefined(employee.lastName) &&
      !this.isNotDefined(employee.phone)
    );
  }
}
