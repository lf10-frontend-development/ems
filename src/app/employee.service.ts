import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Employee} from "./Employee";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private employeeUrl: string = '/backend'

  private httpOptions: object = {
    headers: new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.getToken()}`)
  };

  constructor(private http: HttpClient) {
  }

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.employeeUrl, this.httpOptions);
  }

  /** GET employee by id. Will 404 if id not found */
  getEmployee(id: number): Observable<Employee> {
    const url = `${this.employeeUrl}/${id}`;

    return this.http.get<Employee>(url, this.httpOptions).pipe(
      catchError(this.handleError<Employee>(`getEmployee id=${id}`))
    );
  }

  /** PUT: update the employee on the server */
  updateEmployee(employee: Employee): Observable<any> {
    const url = `${this.employeeUrl}/${employee.id}`;

    return this.http.put(url, employee, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateEmployee'))
    );
  }

  /** POST: create new employee on the server */
  postEmployee(employee: Employee): Observable<any> {
    return this.http.post(this.employeeUrl, employee, this.httpOptions).pipe(
      catchError(this.handleError<any>('postEmployee'))
    )
  }

  private getToken(): string {
    const authUtils = localStorage.getItem('authUtils')
    return JSON.parse(authUtils ?? '{}')._token;
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      return of(result as T);
    };
  }

  deleteEmployee(id: number|undefined):Observable<Employee> {
    if(id === undefined){
      throw new Error('can not delete undefined employee');
    }
    const url = `${this.employeeUrl}/${id}`;

    return this.http.delete<Employee>(url, this.httpOptions);
  }
}
