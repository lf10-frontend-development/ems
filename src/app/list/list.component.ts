import {Component, OnInit} from '@angular/core';
import {EmployeeService} from "../employee.service";
import {Employee} from "../Employee";
import {Location} from "@angular/common"

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: [
    '../app.component.css',
    './list.component.css'
  ]
})
export class ListComponent implements OnInit {

  employees: Employee[] = [];
  filteredEmployees: Employee[] = [];
  searchTerms: string = '';

  constructor(private employeeService: EmployeeService, private location: Location) {
  }

  ngOnInit(): void {
    this.getEmployees();
  }

  onSearchInputChange(searchTerms: string) {
    this.searchTerms = searchTerms;
    this.filterEmployeesBySearchTerm()
  }

  get getFilteredEmployeesSortByIdAsc() {
    return this.filteredEmployees.sort((a: Employee, b: Employee) => {
      if (a.id !== undefined && b.id !== undefined) {
        return (b.id) - (a.id);
      }
      return -1;
    });
  }

  filterEmployeesBySearchTerm(): void {
    this.filteredEmployees = this.employees.filter(employee => employee.firstName?.startsWith(this.searchTerms))
  }

  getEmployees(): void {
    this.employeeService.getEmployees()
      .subscribe(employees => this.filteredEmployees = this.employees = employees);
  }

  deleteEmployee(employee: Employee) {
    this.employees = this.employees.filter(h => h !== employee);
    this.employeeService.deleteEmployee(employee.id).subscribe(() => this.getEmployees());
  }
}
