import {Component, OnInit} from '@angular/core';
import {Employee} from "../Employee";
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {EmployeeService} from "../employee.service";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: [
    '../app.component.css',
    '../app.manageUser.css',
    './detail.component.css'
  ]
})
export class DetailComponent implements OnInit {
  get location(): Location {
    return this._location;
  }

  employee: Employee | undefined | null = null;

  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private _location: Location
  ) {
  }

  ngOnInit(): void {
    this.getEmployee();
  }

  getEmployee(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.employeeService.getEmployee(id)
      .subscribe(employee => {
        this.employee = employee
      });
  }

  redirectToPreviousLocation(): void {
    this._location.back();
  }


}
