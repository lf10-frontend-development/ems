import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  isLoggedIn() {
    this.handleExpired();

    return localStorage.getItem('authUtils') !== null;
  }

  handleExpired() {
    const authUtils = localStorage.getItem('authUtils');
    if (authUtils && Number(JSON.parse(authUtils)._expiresIn) <= Date.now()) {
      localStorage.removeItem('authUtils');
    }
  }
}
