import {Component, OnInit} from '@angular/core';
import {Location} from "@angular/common";
import {Employee} from "../Employee";
import {EmployeeService} from "../employee.service";
import {ValidatorService} from "../validator.service";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: [
    '../app.component.css',
    '../app.manageUser.css',
    './create.component.css',
  ]
})
export class CreateComponent implements OnInit {

  newEmployee: Employee = new Employee();

  constructor(
    private location: Location,
    private employeeService: EmployeeService,
    public validatorService: ValidatorService
  ) {
  }

  ngOnInit(): void {
  }

  redirectToPreviousLocation(): void {
    this.location.back();
  }

  addEmployee(): void {
    this.employeeService.postEmployee(this.newEmployee)
      .subscribe(() => this.redirectToPreviousLocation())
  }
}
